# This master branch is left blank intentionally. Switch to one of the other branches to make changes.


If anyone stumbles apon this repo i highly recommend you NOT run this buid as its highly stripped down and customized. Please get the official build.

### Ubuntu 20.04 (fully updated) is the only tested OS for building any of the branches.

# Useful git commands

> All text surrounded with >< must be typed in without the > or the <, they are just signifying you must substitute any text within with your own input



| git command | Description                    |
| ------------- | ------------------------------ |
| `git clone >repo<`      | This command will clone the git repo to your home directory       |
| `git checkout >branch<`   | This command is used to switch to the desired working branch after cloning    |
| `git add -A`      | This command will add any new files or delete files that were removed from the working branch       |
| `git commit -m ">your message here<"`   | This command will apply any changes done to the working branch.    |
| `git push origin >your working branch<`      | This command will push any commited changes to the git repo hosted on gitlab       |
		
